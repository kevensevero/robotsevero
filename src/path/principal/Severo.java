package principal;
import robocode.*;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Severo - a robot by (Keven Severo)
 */
public class Severo extends AdvancedRobot{
	/**
	 * run: Severo's default behavior
	 */

	int cont = 0; // Contador de turnos em que o radar está procurando o inimigo
	double gunTurnAmt; // Tamanho do giro da arma 
	String NomeInimigo; // Nome do robô encontrado pelo radar


	public void run() {
	
	
	
		setBodyColor(Color.black);
		setGunColor(Color.red);
		setRadarColor(Color.green);
		setScanColor(Color.blue);
		setBulletColor(Color.black);
	
		// Loop Principal
		while(true) {
				
			
			// Vira a arma a procura do inimigo
			turnGunRight(gunTurnAmt);
			// Contador do tempo de busca
			cont++;
			// Se o inimigo não for encontrado em dois turnos, o robô olha para a esquerda
			if (cont > 2) {
				gunTurnAmt = -10;
			}
			// Se o inimigo ainda não for econtrado, gira para a direita
			if (cont > 5) {
				gunTurnAmt = 10;
			}
			// Se o inimigo ainda não for encontrado após 10 turnos, o nome retorna nulo
			if (cont > 11) {
				NomeInimigo= null;
			}
		}	
	}
	

	public void onScannedRobot(ScannedRobotEvent e) {
	
		// Se tivermos um alvo diferente,retorne ao "ScannedRobotEvent"
		
		if (NomeInimigo != null && !e.getName().equals(NomeInimigo)) {
			return;
		}

		if (NomeInimigo == null) {
			NomeInimigo = e.getName(); // Recebe o noome do alvo
		}
		cont = 0;
		// Vira na direção do alvo, quando a distância é maior que 150pxs
		if (e.getDistance() > 150) {
			gunTurnAmt = (e.getBearing() + (getHeading() - getRadarHeading())); // O ângulo do inimigo baseado na minha posição é somado ao resultado da subtração entre a posição (em graus) do inimigo em relação à tela e a direção que o radar está voltado..

			turnGunRight(gunTurnAmt); // Gira a arma na direção do inimigo
			turnRight(e.getBearing()); // Gira o robô pra o ângulo do inimigo encontrado
			ahead(e.getDistance() - 140);
			fire(3);
			return;
		}


		// Quando o inimigo estiver muito próximo, atira com poder de fogo 3, e recua 40 passos;
		if (e.getDistance() < 100) {
			if (e.getBearing() > -90 && e.getBearing() <= 90) {
				fire(3);
				back(40);
			} else {
				ahead(40);
			}
		}
		scan();
	}

	public void onHitByBullet(HitByBulletEvent e) {
		// Ao ser atingido por uma bala, ele se vira à esquerda, e volta 20px num mesmo turno
		
		setBack(20);
		setTurnLeft(45);
		
		execute();
	}
	
	public void onHitWall(HitWallEvent e) {
		// Ao bater na parede ele se vira 45º à direita em um turno, e avança 200px no próximo
		turnRight(45);
		ahead(200);
	}	
	
	public void onHitRobot(HitRobotEvent e){
	
		if (e.getBearing() > -90 && e.getBearing() < 90) {
		// Ele dispara com força 2 e recua, caso o inimigo esteja à sua frente
			fire(2);
			back(100);	
		} 
		else {
			// Ele avança, caso o inimigo esteja atrás
			ahead(50);
		}

	}
}
