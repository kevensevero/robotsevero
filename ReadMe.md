## ReadMe for Severo Robot

Updated 28-10-2020 by Keven Severo

Robocode Home Page:
[https://robocode.sourceforge.io/](https://robocode.sourceforge.io/)

### TABLE OF CONTENTS


1.  [Main](#principal)
2.  [Scan](#scan)
3.  [Hit Bullet](#hit-bullet)
4.  [Hit Robot](#hit-robot)
5.  [Considerações](#considerações)



### 1. PRINCIPAL

O metódo principal inicia setando as cores ao robô, radar, e arma. Inicia também o loop infinito definido por 'while'  que inicia a busca inicial ao inimigo variando a direção de sua varredura a partir de operadores lógicos "if", que define sua mudança de direção com base na variável 'cont' que representa o número de turnos decorrentes. Se após 10 turnos o inimigo não for encontrado, atibrui-se à váriavel 'NomeInimigo' o valor nulo.


**Planejamento e Codificação**

A ideia inicial foi a de fazer o movimento primário e principal de maneira circular, pois me parecia o melhor jeito de fazer uma varredura maior no campo. Minha primeira versão possui essa movimentação, mas ela acaba não sendo muito eficiente com robôs que se movimentem em sentindo contrário, ou que tenham movimentação com bastante variação de direção, pois estes sempre fogem ao radar, ou desviam dos disparos sem muito esforço, além de quê na possibilidade das batalhas serem feitas em grupo, o movimento circular o deixaria muito vulnerável aos adversários, já que eles estariam dispostos de maneira mais abrangente no campo. O código da versão final é uma adaptação do robô 'Tracker', pois notei bastante eficiência em sua varredura, que acaba não apresentando muita facilidade em receber o primeiro disparo do adversário.

### 2. SCAN

O evento também tem sua lógica montade sob a condicional 'if'. 

- O primeiro 'if' retorna quando a variável 'NomeInimigo' não está vazia, e o nome recebido a partir do radar, é o mesmo guardado dentro de 'NomeInimigo' (isso significa que o inimigo encontrado ainda dentro do movimento principal do meu robô, é o mesmo encontrado pelo radar dentro do 'ScannedRobotEvent'). 
- O segundo 'if' atribui valor à 'NomeInimigo' se ela ainda estiver vazia. 
- O terceiro 'if' gira o robô, e a arma na direção onde foi encontrado o inimigo, caso a distância até ele seja maior que 150px, caminha em sua direção com o valor de 140px, e dispara com força *3*
- O quarto 'if'dispara com força 3, e percorre 40pxs para trás quando o inimigo estiver a uma distância menor que 100pxs, e a um ângulo entre (-90) e (90). Caso a condição não se sustente, o robô avança 40pxs afim de se aproximar do inimigo.
- Na saída do 'if' o "scan" é chamado novamente

**Planejamento e Codificação**
Ao planejar o comportamento do robô, antes de qualquer codificação, pensei em fazê-lo fixar seu radar assim que encontrasse um inimigo, de maneira a perseguí-lo com precisão para fazer os disparos. Ao assistir a algumas disputas no youtube, vi que era um comportamento possível de ser programado, mas que o nível de automação é de uma complexidade que não pude dominar dentro do tempo disponível pra codificação. A minha primeira versão não girava a arma de maneira independente ao corpo do robô, justamente para que suas movimentações fossem únicas, afim de facilitar a identificação, e programação de movimentações a partir dos eventos tanto de 'onScannedRobot', como de 'onHitBullet'.

### 3. HIT BULLET

Ao ser atingido por um disparo do inimigo, o robô recua 20px simuntaneamente ao movimento de giro 45º à direita afim de desviar de possíveis disparos consecutivos.

**Planejamento e Codificação**

Percebi um bug nos meus códigos iniciais que travavam o robô sempre que ele colidia com a parede ao mesmo tempo em que era atingido por um disparo. Encontrei um metódo na biblioteca pra priorizar eventos, porém, o mesmo só funciona com a divisão de eventos por classes, a partir de OO, portanto, acabei encontrando numa combinação de movimentos simuntâneos (o recuo, e o giro à esquerda) a solução pra que o robô não travasse em um loop de colisões com a parede, que acontecia principalmente nas quinas do campo.
### 4. HIT ROBOT

 Importei uma parte do código do robô 'Tracker' que usa a condicional 'if' pra verificar a posição do robô inimigo no momento da colisão, e caso ele esteja de frente, meu robô dispara com força 2 e recua 100pxs. Caso o inimigo colida a partir de uma outra posição, meu robô avança 50pxs
 
 **Planejamento e Codificação**
Na versão inicial eu apenas disparava com força 3, mas percebi que esse disparo muitas vezes não era dado na direção do robô, pois a colisão podia acontecer lateralmente, ou até mesmo de fundo.


### 5. CONSIDERAÇÕES 

Não tive grandes dificuldades em planejar as movimentações, e reações do robô aos eventos. A biblioteca de metódos vasta facilitou alguns gatilhos de projeções, principalmente no que diz respeito ao scan. Encontrei alguns metódos na biblioteca .math que seriam muito utéis caso eu tivesse mais tempo para estudá-las e aplicá-las afim de fazer um código mais enxuto, e mais eficaz no rastreamento dos adversários, além de alguns metódos na própria biblioteca do robocode que poderiam ser usados para a contrução de uma movimentação mais eficaz para disputas com mais de um adversário em campo. Considero um resultado bacana o que consegui produzir com o conhecimento precário que tenho de JAVA, e curti pra caramba poder ver algo funcionando a partir de uma codificação minha. Os desafios da Solutis, de uma maneira geral tem engatilhado um movimento de auto-conhecimento bem gratificante por aqui. Espero poder estar com vocês nas próximas fases, e aprender bastante dentro desse prosel.
